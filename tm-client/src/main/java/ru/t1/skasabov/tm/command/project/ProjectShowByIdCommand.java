package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.skasabov.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull private static final String NAME = "project-show-by-id";

    @NotNull private static final String DESCRIPTION = "Display project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        showProject(response.getProject());
    }

}
