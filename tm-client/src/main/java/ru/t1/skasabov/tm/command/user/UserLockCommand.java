package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.UserLockRequest;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class UserLockCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "user-lock";

    @NotNull private static final String DESCRIPTION = "User lock.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(login);
        getUserEndpoint().lockUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMIN};
    }

}
