package ru.t1.skasabov.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.command.ISystemConnectCommand;
import ru.t1.skasabov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationVersionResponse;

@NoArgsConstructor
public final class ApplicationVersionCommand extends AbstractSystemCommand implements ISystemConnectCommand {

    @NotNull private static final String NAME = "version";

    @NotNull private static final String DESCRIPTION = "Show program version.";

    @NotNull private static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

}
