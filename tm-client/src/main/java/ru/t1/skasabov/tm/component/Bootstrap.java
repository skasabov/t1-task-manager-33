package ru.t1.skasabov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.skasabov.tm.api.command.ISystemConnectCommand;
import ru.t1.skasabov.tm.api.endpoint.*;
import ru.t1.skasabov.tm.api.repository.ICommandRepository;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.client.*;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.command.server.ConnectCommand;
import ru.t1.skasabov.tm.command.server.DisconnectCommand;
import ru.t1.skasabov.tm.exception.system.ArgumentEmptyException;
import ru.t1.skasabov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.skasabov.tm.exception.system.CommandEmptyException;
import ru.t1.skasabov.tm.exception.system.CommandNotSupportedException;
import ru.t1.skasabov.tm.repository.CommandRepository;
import ru.t1.skasabov.tm.service.*;
import ru.t1.skasabov.tm.util.SystemUtil;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull private static final String PACKAGE_COMMANDS = "ru.t1.skasabov.tm.command";

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull private final ILoggerService loggerService = new LoggerService();

    @NotNull private final IPropertyService propertyService = new PropertyService();

    @NotNull private final IAuthEndpoint authEndpoint = new AuthEndpoint();

    @NotNull private final ISystemEndpoint systemEndpoint = new SystemEndpoint();

    @NotNull private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull private final IDomainEndpoint domainEndpoint = new DomainEndpoint();

    @NotNull private final IUserEndpoint userEndpoint = new UserEndpoint();

    @NotNull private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "tm-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    public static void close() {
        System.exit(0);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) close();
        processCommands();
    }

    private void processCommands() {
        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            }
            catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
            loggerService.argument(argument);
        }
        catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
        return true;
    }

    public void processCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) throw new CommandEmptyException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentEmptyException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        if (abstractCommand instanceof ISystemConnectCommand) processCommand(ConnectCommand.NAME);
        abstractCommand.execute();
        if (abstractCommand instanceof ISystemConnectCommand) processCommand(DisconnectCommand.NAME);
    }

}
