package ru.t1.skasabov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.DataJsonSaveJaxBRequest;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull private static final String NAME = "data-save-json-jaxb";

    @NotNull private static final String DESCRIPTION = "Save data to json file.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest();
        getDomainEndpoint().saveDataJsonJaxB(request);
    }

}
