package ru.t1.skasabov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.client.*;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull public static final String NAME = "connect";

    @NotNull private static final String DESCRIPTION = "Connect to server.";

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final AuthEndpoint authEndpoint = (AuthEndpoint) getServiceLocator().getAuthEndpoint();
        @Nullable final ProjectEndpoint projectEndpoint = (ProjectEndpoint) getServiceLocator().getProjectEndpoint();
        @Nullable final TaskEndpoint taskEndpoint = (TaskEndpoint) getServiceLocator().getTaskEndpoint();
        @Nullable final UserEndpoint userEndpoint = (UserEndpoint) getServiceLocator().getUserEndpoint();
        @Nullable final DomainEndpoint domainEndpoint = (DomainEndpoint) getServiceLocator().getDomainEndpoint();
        @Nullable final SystemEndpoint systemEndpoint = (SystemEndpoint) getServiceLocator().getSystemEndpoint();
        authEndpoint.connect();
        @Nullable final Socket socket = authEndpoint.getSocket();
        projectEndpoint.setSocket(socket);
        taskEndpoint.setSocket(socket);
        userEndpoint.setSocket(socket);
        domainEndpoint.setSocket(socket);
        systemEndpoint.setSocket(socket);
    }

}
