package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.dto.request.UserLoginRequest;
import ru.t1.skasabov.tm.dto.request.UserLogoutRequest;
import ru.t1.skasabov.tm.dto.request.UserProfileRequest;
import ru.t1.skasabov.tm.dto.response.UserLoginResponse;
import ru.t1.skasabov.tm.dto.response.UserLogoutResponse;
import ru.t1.skasabov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final AbstractEndpoint client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
