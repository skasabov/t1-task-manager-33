package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskGetByIdRequest;
import ru.t1.skasabov.tm.dto.response.TaskGetByIdResponse;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-show-by-id";

    @NotNull private static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(id);
        @NotNull final TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        showTask(response.getTask());
    }

}
