package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-unbind-from-project";

    @NotNull private static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(projectId, taskId);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

}
