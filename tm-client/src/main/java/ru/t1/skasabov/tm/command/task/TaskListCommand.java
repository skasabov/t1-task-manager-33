package ru.t1.skasabov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.request.TaskListRequest;
import ru.t1.skasabov.tm.dto.response.TaskListResponse;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@NoArgsConstructor
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull private static final String NAME = "task-list";

    @NotNull private static final String DESCRIPTION = "Show list tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(sort);
        @NotNull final TaskListResponse response = getTaskEndpoint().listTasks(request);
        renderTasks(response.getTasks());
    }

}
