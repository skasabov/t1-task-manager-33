package ru.t1.skasabov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.client.AuthEndpoint;
import ru.t1.skasabov.tm.command.AbstractCommand;
import ru.t1.skasabov.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull public static final String NAME = "disconnect";

    @NotNull private static final String DESCRIPTION = "Disconnect from server.";

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final AuthEndpoint authEndpoint = (AuthEndpoint) getServiceLocator().getAuthEndpoint();
        authEndpoint.disconnect();
    }

}
