package ru.t1.skasabov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull private static final String NAME = "change-user-password";

    @NotNull private static final String DESCRIPTION = "Change password of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
