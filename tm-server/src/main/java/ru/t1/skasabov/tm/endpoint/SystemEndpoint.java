package ru.t1.skasabov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.util.FormatUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getPropertyService();
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationSystemInfoResponse getSystemInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ApplicationSystemInfoRequest request
    ) {
        @NotNull final ApplicationSystemInfoResponse response = new ApplicationSystemInfoResponse();
        @NotNull final Runtime runtime = Runtime.getRuntime();
        response.setAvailableProcessors(runtime.availableProcessors());
        final long freeMemory = runtime.freeMemory();
        response.setFreeMemory(FormatUtil.format(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        response.setMaximumMemory(maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        response.setTotalMemory(FormatUtil.format(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        response.setUsageMemory(FormatUtil.format(usageMemory));
        return response;
    }

}
