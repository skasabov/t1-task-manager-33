package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    public TaskEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public TaskGetByProjectIdResponse findAllByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        return call(request, TaskGetByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTasks(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskListResponse listTasks(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    public static void main(@Nullable final String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());

        @NotNull final TaskEndpointClient taskClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskClient.createTask(new TaskCreateRequest("HELLO", "WORLD")));
        System.out.println(taskClient.listTasks(new TaskListRequest()).getTasks());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
