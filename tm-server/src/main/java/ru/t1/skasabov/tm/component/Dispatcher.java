package ru.t1.skasabov.tm.component;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.Operation;
import ru.t1.skasabov.tm.dto.request.AbstractRequest;
import ru.t1.skasabov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class Dispatcher {

    @NotNull private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        @NotNull final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
