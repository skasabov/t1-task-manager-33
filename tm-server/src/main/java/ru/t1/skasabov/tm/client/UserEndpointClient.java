package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    public UserEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    public static void main(@Nullable final String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        try {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final UserEndpointClient userClient = new UserEndpointClient(authEndpointClient);
            userClient.lockUser(new UserLockRequest("user"));
        }
        catch (@NotNull final RuntimeException e) {
            System.out.println(e.getMessage());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
            @NotNull final UserEndpointClient userClient = new UserEndpointClient(authEndpointClient);
            userClient.lockUser(new UserLockRequest("user"));
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final UserEndpointClient userClient = new UserEndpointClient(authEndpointClient);
            userClient.updateUserProfile(new UserUpdateProfileRequest("test", "test", "test"));
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
