package ru.t1.skasabov.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.response.ApplicationErrorResponse;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.exception.server.SocketNotFoundException;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient {

    @NotNull private String host = "localhost";

    @NotNull private Integer port = 6060;

    @Nullable private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @SneakyThrows
    @NotNull
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @SneakyThrows
    @NotNull
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @SneakyThrows
    @NotNull
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @SneakyThrows
    @NotNull
    private OutputStream getOutputStream() {
        if (socket == null) throw new SocketNotFoundException();
        return socket.getOutputStream();
    }

    @SneakyThrows
    @NotNull
    private InputStream getInputStream() {
        if (socket == null) throw new SocketNotFoundException();
        return socket.getInputStream();
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        if (socket == null) return;
        socket.close();
    }

}
