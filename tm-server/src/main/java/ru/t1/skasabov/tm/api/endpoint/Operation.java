package ru.t1.skasabov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.AbstractRequest;
import ru.t1.skasabov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
