package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.component.ISaltProvider;
import ru.t1.skasabov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

}
