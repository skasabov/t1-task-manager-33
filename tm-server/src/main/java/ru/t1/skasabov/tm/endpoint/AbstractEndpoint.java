package ru.t1.skasabov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.service.IServiceLocator;
import ru.t1.skasabov.tm.api.service.IUserService;
import ru.t1.skasabov.tm.dto.request.AbstractUserRequest;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.user.PermissionException;
import ru.t1.skasabov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new PermissionException();
        if (role == null) throw new PermissionException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new PermissionException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new PermissionException();
        @Nullable final Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new PermissionException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new PermissionException();
    }

}
