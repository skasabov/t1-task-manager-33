package ru.t1.skasabov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    private void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    private void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

}
