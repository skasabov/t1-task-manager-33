package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(@NotNull final ApplicationSystemInfoRequest request) {
        return call(request, ApplicationSystemInfoResponse.class);
    }

    public static void main(@Nullable final String[] args) {
        ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance();
        @NotNull final ApplicationAboutResponse aboutResponse = systemEndpoint.getAbout(
                new ApplicationAboutRequest()
        );
        @NotNull final ApplicationVersionResponse versionResponse = systemEndpoint.getVersion(
                new ApplicationVersionRequest()
        );
        @NotNull final ApplicationSystemInfoResponse systemInfoResponse = systemEndpoint.getSystemInfo(
                new ApplicationSystemInfoRequest()
        );
        System.out.println("[ABOUT]");
        System.out.println("Name: " + aboutResponse.getName());
        System.out.println("Email: " + aboutResponse.getEmail());
        System.out.println();
        System.out.println("[VERSION]");
        System.out.println(versionResponse.getVersion());
        System.out.println();
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + systemInfoResponse.getAvailableProcessors());
        System.out.println("Free memory: " + systemInfoResponse.getFreeMemory());
        System.out.println("Maximum memory: " + systemInfoResponse.getMaximumMemory());
        System.out.println("Total memory: " + systemInfoResponse.getTotalMemory());
        System.out.println("Usage memory: " + systemInfoResponse.getUsageMemory());
    }

}
