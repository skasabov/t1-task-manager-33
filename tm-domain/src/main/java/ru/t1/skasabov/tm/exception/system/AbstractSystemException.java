package ru.t1.skasabov.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull String message) {
        super(message);
    }

    public AbstractSystemException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @NotNull String message,
            @NotNull Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
