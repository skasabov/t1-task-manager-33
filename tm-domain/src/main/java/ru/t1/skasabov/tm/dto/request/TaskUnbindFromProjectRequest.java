package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable private String projectId;

    @Nullable private String taskId;

    public TaskUnbindFromProjectRequest(@Nullable final String projectId, @Nullable final String taskId) {
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
