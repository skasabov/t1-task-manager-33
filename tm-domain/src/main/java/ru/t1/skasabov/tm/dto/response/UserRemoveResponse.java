package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.User;

@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}
