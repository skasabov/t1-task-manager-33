package ru.t1.skasabov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

}
