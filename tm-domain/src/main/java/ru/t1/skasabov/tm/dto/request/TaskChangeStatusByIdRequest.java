package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable private String id;

    @NotNull private Status status;

    public TaskChangeStatusByIdRequest(@Nullable final String id, @NotNull final Status status) {
        this.id = id;
        this.status = status;
    }

}
