package ru.t1.skasabov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.exception.field.StatusEmptyException;
import ru.t1.skasabov.tm.exception.field.StatusIncorrectException;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) throw new StatusEmptyException();
        for (@NotNull final Status status: values()) {
            if (status.name().equals(value)) return status;
        }
        throw new StatusIncorrectException();
    }

    @NotNull private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
