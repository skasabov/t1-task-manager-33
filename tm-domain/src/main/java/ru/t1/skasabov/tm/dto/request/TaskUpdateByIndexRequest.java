package ru.t1.skasabov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @NotNull private Integer index;

    @Nullable private String name;

    @Nullable private String description;

    public TaskUpdateByIndexRequest(
            @NotNull final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        this.index = index;
        this.name = name;
        this.description = description;
    }

}
