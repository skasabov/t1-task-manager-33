package ru.t1.skasabov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.model.IWBS;
import ru.t1.skasabov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull private String name = "";

    @Nullable private String description = "";

    @NotNull private Status status = Status.NOT_STARTED;

    @Nullable private String projectId;

    @NotNull private Date created = new Date();

    @Nullable private Date dateBegin;

    @Nullable private Date dateEnd;

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
